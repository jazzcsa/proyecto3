
package proyecto1;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import  java.util.Stack;
import java.util.Collections;


public class Proyecto1 {
    //pila
    Stack<Integer> S;
    Stack<Integer> ExP;
    Stack<Integer> S2;
    Stack<Integer> Ncercanos;
    List area = new ArrayList();
    List orden = new ArrayList();
    int tlista ;
    int caidfs = 0;
    int mvalorA = 100;
    int mvalorN = 0;
    int Aristamas_corta;
    int valn;
    int nodo_cercano ;
    // variables generales
    Random rm= new Random();
    Random peso = new Random();
    SecureRandom pr =new SecureRandom();
    int nDN ;
    int nDV ;
    double p;
    float dU ,d;
    String gP = " " ; 
    public int nodo_start;
    public int nodo_buscar;
 
      grafo gdikstra = new grafo();
    int cndfsr = 0;
    int nodoa;
    
    
    // se decalran varios contructores
    

   //esta parte es para dijkstra
        int nMAgdij  ;
      
        nodo VN7[] ;
        arista AN7[] ;
        arista ANDF7[] ;
   //hasta aqui dijkstra 
        

   public Proyecto1()
   {  
   } 
   public Proyecto1(int a,int  b)      
   {
       this.nDN = a;
       this.nDV = b; 
   }
    public Proyecto1(int a,double b)       
   {
       this.nDN = a;
       this.p = b;  
   }
     public Proyecto1(int a,float b)       
   {
       this.nDN = a;
       this.dU = b;  
   }
     public Proyecto1 (int a)
     {
         this.nDN=a;
         
         
     }
             
            

//**********************************************************************
//*************************************************************************
//********************************************************************
 //funcion para ingresar abyacentes a pila
   public  void abyacentes2()
   {
      tlista = area.size();
      
      for (int f = 0;f<tlista;f++)
      {       
           valn=(int)area.get(f);
         
           for(int m = 0; m<AN7.length; m++)
                {
                  if(AN7[m].existe)
                        {

                            if(VN7[valn].nN == AN7[m].n1 & VN7[AN7[m].n2-1].ex == false )
                            {
                              
                              
                                if(AN7[m].elegida == false)
                                {
                                    Ncercanos.add(VN7[AN7[m].n2-1].nN);
                                   VN7[AN7[m].n2-1].distancia = VN7[AN7[m].n1-1].distancia + AN7[m].peso;
                              S2.add(AN7[m].nA);
                                }
                            }
                            else if(  VN7[valn].nN == AN7[m].n2 & VN7[AN7[m].n1-1].ex == false)
                            {
                              
                               if(AN7[m].elegida == false )
                                {
                                    Ncercanos.add(VN7[AN7[m].n1-1].nN);
                                  VN7[AN7[m].n1-1].distancia = VN7[AN7[m].n2-1].distancia + AN7[m].peso;
                               S2.add(AN7[m].nA);
                                }
                               
                               
                                
                            } 

                        }  
                }    
      } 
      for (int i =0;i<nDN;i++)
      {
          if (VN7[i].ex == false)
          {
          orden.add(VN7[i].distancia);
          }
      }
      Collections.sort(orden);
      
   }
   
   int dijkstra ()
   {
       //variable para indicar el nodo de comienzo
        int na = 0;
           // variabel para indicar a que nodo se conecta el nodo de comienzo
        int nd = 0;
      
        int arista ;
        String nodor="";
        nMAgdij = (nDN *(nDN-1))/2;
        VN7 = new nodo[nDN];
        AN7  = new arista[nMAgdij];
        ANDF7  = new arista[nMAgdij];
        //
        S2 = new Stack<Integer>();
        Ncercanos  = new Stack<Integer>();
        ExP = new Stack<Integer>();
        // se declaran los objetos nodo y arsita que sao vectores
      
// esto es para imprimir *********************************************************
         
         File archivo ;
         PrintWriter escribir ;
        
         try {
        archivo = new File("dijkstra.gv"); 
        
        if (archivo.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
 //hastaaqui se contruye el archivo**************************************************
        // se contruyen todos los nodos
       for(int i = 0; i < nDN ; i++)
       { 
           VN7[i] = new nodo (i+1);
           
           
       }  
       // se calcula el numero maximo de aristas posibles
      for(int i =0;i<nMAgdij;i++)
      {
          
          AN7[i] = new arista();
          ANDF7[i] = new arista();
      }
       // se contruyen las aristas
       for (int i=0; i<nMAgdij; i++)
       {
           
           
           // se aignan los nodos de la arista      
           AN7[i].n1 = VN7[na].nN;
           AN7[i].n2 = VN7[nd].nN;
           AN7[i].peso = peso.nextInt(100);
           AN7[i].nA = i+1;
           // se verifica si la arista es con el mimo nodo
           if (AN7[i].n1 == AN7[i].n2)
           {
               i--;
           }
           else if(pr.nextDouble()<0.6)
           {
               
               AN7[i].existe = true;
           }
            for(int j = 0; j<i;j++)
           {
               if((AN7[i].n1 == AN7[j].n1 &  AN7[i].n2 == AN7[j].n2)|(AN7[i].n1 == AN7[j].n2 &  AN7[i].n2 == AN7[j].n1))
               {
                   i--;
                   AN7[i].existe = false;
               }
           }         
           if(nd<nDN-1)
           {
           nd=nd+1;              
           }
           else
           {
               nd = 0;
               na++;
           }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
          VN7[na].Used = true;
          VN7[nd].Used = true;  
       }
       
       
 // esta parte solo es para imprimir en pantallla*************************
 // ademas se contruye el grafo
       System.out.println("\ngraph Guil {");
       //crea grafo con aristas primero
       for(int i = 0; i<nMAgdij; i++ )
       {
           if(AN7[i].existe)
           {
          
           System.out.println(AN7[i].n1 + " -- " +AN7[i].n2 + ";" + "peso: " + AN7[i].peso);
           }
       } 
       
          
 //hasta aqui es para imprimir en pantalla********************************
    nodo_start  = rm.nextInt(nDN);
    VN7[nodo_start].distancia = 0;
    VN7[nodo_start].ex = true;
    nodo_buscar = rm.nextInt(nDN);
    
   
   
       System.out.println("el nodo de inicio es:" + VN7[nodo_start].nN);
       System.out.println("el nodo a buscar es:"+VN7[nodo_buscar].nN);
    nodor = nodor + VN7[nodo_start].nN;
  
//aqui se hace la parte iterativa del grafo
area.add(nodo_start);
      
//aqui se hace la parte iterativa del grafo
for ( int i=0; i < nDN;i++)
{
    
    if (area.contains(nodo_buscar))
    {
       
        i = nDN;
       
        
    }
    else{
    abyacentes2();

  
 while(!S2.empty())
 {
    
     mvalorN = S2.pop();
     nodo_cercano = Ncercanos.pop();
 
     if(VN7[nodo_cercano-1].distancia == (int)orden.get(0))
             {
                 if (VN7[nodo_cercano-1].ex)
                 {
                    //no se ejecuta nada cambiar adespues a negado
                 }
                 else{
                area.add(VN7[nodo_cercano-1].nN -1);
                Aristamas_corta = mvalorN;
                VN7[nodo_cercano-1].ex = true;
                ExP.add(mvalorN);
                 }
               
               
        
             }
     
    
 }
        
 orden.clear();

    }
 
 }
       System.out.println(ExP);
//esto es para crear el grafo
while(!ExP.empty())
{
    arista = ExP.pop();
    
    if (AN7[arista-1].n1 == VN7[nodo_buscar].nN)
    {
        
     gdikstra.grafo = gdikstra.grafo + AN7[arista-1].n1 +"("+VN7[AN7[arista-1].n1-1].distancia+")"+ " -- " + AN7[arista-1].n2 + ";\n" ;    
    }
    else if (AN7[arista-1].n2 == VN7[nodo_buscar].nN)
    {
        
         gdikstra.grafo = gdikstra.grafo + AN7[arista-1].n1 +"("+VN7[AN7[arista-1].n1-1].distancia+")"+ " -- " + AN7[arista-1].n2  +"("+VN7[AN7[arista-1].n2-1].distancia+")"+ ";\n" ;
    }
   else
    {
        
       gdikstra.grafo = gdikstra.grafo + AN7[arista-1].n1 + " -- " + AN7[arista-1].n2 + ";\n" ; 
    }
    
}


       System.out.println(area);
       System.out.println(VN7[nodo_buscar].distancia);
    

//hasta aqui parte iterativa del grafo
   
 
       
        try {
        escribir =  new PrintWriter("dijkstra.gv", "utf-8") ;
     
        escribir.println("\ngraph bfsi {");
        escribir.println(gdikstra.grafo); 
 
        escribir.println("\n}");
        escribir.close();
  
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
 //hasta aqui el archivo del grafo****************************************
    
      return 0 ;
      
      
   }
   //hasta aqui idfs********************************************************
   
}


